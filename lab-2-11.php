<HTML>
<TITLE> Дружественные числа </TITLE>
<BODY>
<TABLE border=1>
<?php

$n = rand();
$m = rand();

function sum($a) //Функция для подсчета суммы всех делителей числа
{
$res = 0;
for ($i = 2; $i <= sqrt($a); $i++) {
if ($a % $i == 0) {
if ($i == ($a / $i))
$res += $i;
else
$res += ($i + $a / $i); } }
return ($res + 1); }

function dr($x, $y) { //Функция, которая определяет равна ли сумма делителей обоих чисел
if (sum($x) != $y)
return false;
return (sum($y) == $x); }

if (dr($n, $m))
echo("Числа " . $n . " и " . $m . " являются дружественными" );
else
echo("Числа " . $n . " и " . $m . " не являются дружественными" );
?>
</TABLE>
</BODY>
</HTML>